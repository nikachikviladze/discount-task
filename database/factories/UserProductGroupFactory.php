<?php

namespace Database\Factories;

use App\Models\User;
use App\Models\UserProductGroup;
use Illuminate\Database\Eloquent\Factories\Factory;

class UserProductGroupFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = UserProductGroup::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => User::all()->random()->id,
            'discount' => rand(1,100),
        ];
    }
}
