<?php

namespace Database\Seeders;

use App\Models\ProductGroupItem;
use App\Models\UserProductGroup;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserProductGroupSeeder extends Seeder
{
    public function data($number){

        UserProductGroup::factory()->has(ProductGroupItem::factory()->count($number))->create();

    }
    public function run()
    {
        $number = rand(2,4);

        $this->data($number);

        $duplicated = DB::table('product_group_items')
                    ->select('product_id', DB::raw('count(`product_id`) as id'))
                    ->groupBy('product_id')
                    ->having('id', '>', 1)
                    ->pluck('product_id')
                    ->toArray();

        ProductGroupItem::whereIn('product_id', $duplicated)->delete();

        if(ProductGroupItem::count()==0){
            $this->data(1);
        }
    }
}
