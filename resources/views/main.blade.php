@extends('layouts.app')
@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            @if(auth()->user()->UserProductGroup)

            <div class="alert alert-info">
                Discount {{auth()->user()->UserProductGroup->discount}}%. Discount products for you: 
                <ul>
                    @foreach(auth()->user()->discountProducts() as $product)
                    <li> {{$product->title}} - <span class="">{{$product->price}}</span></li>
                    @endforeach
                </ul>
                Discount works if all prodacts will be selected
            </div>

            @endif


            <div class="mb-2">
                <h2>My Cart</h2>
                <table class="table">
                    <thead>
                      <tr>
                        <th scope="col">#</th>
                        <th scope="col">Product</th>
                        <th scope="col">Price</th>
                        <th scope="col">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                        <tr v-for="(product, index) in cart.products">
                            <th scope="row">@{{product.product_id}}</th>
                            <td>@{{product.title}} </td>
                            <td>@{{product.price}}</td>
                            <td> 
                                <input 
                                    v-model="cartProduct[product.product_id]"                                    
                                    type="number"  min="1" type="text" class="form-control w-25 d-inline">
                            
                                <button @click.prevent="updateCart(product)" class="btn btn-secondary" type="button">Update Cart</button> 
                                <button @click.prevent="deleteCartItem(product.product_id)" class="btn btn-danger" type="button">Remove from Cart</button> 
                            </td>
                        </tr>
                    </tbody>
                </table>    
                <div>Discount: @{{cart.discount}}</div>
            </div>

            <h2>All products</h2>
            <table class="table">
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Product</th>
                    <th scope="col">Price</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                <tbody>
                
                  <tr v-for="product in products">
                    <th scope="row">@{{product.product_id}}</th>
                    <td>@{{product.title}} </td>
                    <td>@{{product.price}}</td>
                    <td><button @click.prevent="addcart(product.product_id)" class="btn btn-secondary" type="button">Add to Cart</button> </td>
                  </tr>

                </tbody>
            </table>    

        </div>
    </div>
</div>

@endsection