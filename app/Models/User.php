<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function Product()
    {
        return $this->hasMany(Product::class, 'user_id');
    }
    public function Cart()
    {
        return $this->hasMany(Cart::class, 'user_id');
    }
    public function UserProductGroup()
    {
        return $this->hasOne(UserProductGroup::class, 'user_id');
    }
    public function discountProducts()
    {
        return $this->UserProductGroup->ProductGroupItem()->join('products', 'product_group_items.product_id', 'products.product_id')->get();
    }
}
