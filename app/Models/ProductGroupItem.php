<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductGroupItem extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $primaryKey = 'item_id';

    public function UserProductGroup()
    {
        return $this->belongsTo(UserProductGroup::class);
    }

    public function Product()
    {
        return $this->hasOne(Product::class, 'product_id');
    }

}
