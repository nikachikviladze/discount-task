<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    public $timestamps = false;

    protected $primaryKey = 'product_id';

    protected $fillable = [
        'title',
        'price',
    ];

    public function User()
    {
        return $this->belongsTo(User::class);
    }

    public function items()
    {
        return $this->belongsTo(ProductGroupItem::class);
    }

    public function Cart()
    {
        return $this->hasOne(Cart::class);
    }


}
