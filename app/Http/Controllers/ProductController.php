<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductStoreRequest;
use App\Http\Requests\ProductUpdateRequest;
use App\Http\Resources\ProductResource;
use App\Models\Cart;
use App\Models\Product;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    public $userId;

    public function __construct()
    {        
        $this->middleware(function ($request, $next) {
            $this->userId = Auth::user()->id;
            return $next($request);
        });
    }

    public function index()
    {
        return view('main');
    }
    public function products()
    {
        return ProductResource::collection(Product::all());
    }
    public function getUserCart()
    {
        $discountPrice = 0;
        $ids = [];
        
        $user = User::find($this->userId);
        $group = $user->UserProductGroup;

        if($group){
            $products = $group->ProductGroupItem;
            $ids = $products->pluck('product_id')->toArray();
        }

        $cart = $user->cart()
                    ->join('products', 'carts.product_id', 'products.product_id')
                    ->select(['carts.product_id', 'carts.quantity', 'products.price', 'products.title'])
                    ->get();

        $discountProducts = $cart->whereIn('product_id', $ids);

        if ($discountProducts->count() == count($ids) && $discountProducts->isNotEmpty() ) {

            $arr = $discountProducts->groupBy('quantity')->toArray();
            $min = min(array_keys($arr));

            $Percentage = $group->discount;

            $price = $discountProducts->sum('price');

            $discountPrice =sprintf("%.2f", (($price/100)*$Percentage)*$min);

        }

        return ['products'=>ProductResource::collection($cart), 'discount'=>$discountPrice];           
    }

    public function addProductInCart(ProductStoreRequest $request)
    {
        Cart::firstOrCreate(['user_id'=>$this->userId,'product_id'=>$request->product_id]);

        return $this->getUserCart();
    }
    public function setCartProductQuantity(ProductUpdateRequest $request)
    {
        Cart::where("product_id", $request->product_id)
            ->where('user_id', $this->userId)
            ->update(['quantity'=>$request->quantity]);

        return $this->getUserCart();
    }
    public function removeProductFromCart($id)
    {
        $product = Cart::where("product_id", $id)->where("user_id", $this->userId)->first();
        
        $product->delete();

        return $this->getUserCart();
    }

}