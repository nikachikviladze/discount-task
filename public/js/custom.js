const app = new Vue({
    el: '#app',

    data() {
        return{
            products:{},
            cart:{},
            cartProduct: []
    
        }
    },
    mounted(){
        this.fetchProducts()
        this.fetchCart()
        
    },
    methods:{

        fetchProducts(){
            axios.get(`/api/products`).then(resp=>{
                this.products =resp.data.data
            })
        },
        // Cart 
        cartData(resp){
            this.cart =resp.data

            for (let index = 0; index < this.cart.products.length; index++) {
                product = this.cart.products[index]
                this.cartProduct[product.product_id] = product.quantity
            }
        },        
        fetchCart(){
            axios.get(`/getUserCart`).then(resp=>{
                this.cartData(resp)
            })
        },
        addcart(product){
            axios.post(`/addProductInCart`, {product_id:product}).then(resp=>{
                this.cartData(resp)                
            })
        },          
        updateCart(product){
            var quantity = this.cartProduct[product.product_id]

            axios.post(`/setCartProductQuantity`, {product_id:product.product_id, quantity:quantity}).then(resp=>{
                this.cartData(resp)
            })
        },
        deleteCartItem(id){
            axios.delete(`/removeProductFromCart/${id}`).then(resp=>{
                this.cartData(resp)
            })
        }
    }
});
