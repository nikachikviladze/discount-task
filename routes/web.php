<?php

use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\Route;


Route::controller(ProductController::class)->group(function () {

    Route::group(['middleware' => ['auth']], function () {
        Route::get('/', 'index');
        Route::get('getUserCart', 'getUserCart')->middleware('auth');
        
        Route::post('addProductInCart', 'addProductInCart');
        Route::post('setCartProductQuantity',  'setCartProductQuantity');
        Route::delete('removeProductFromCart/{id}', 'removeProductFromCart');
    });
    
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
